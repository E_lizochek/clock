# Analog Clock (React js)

![](https://gitlab.com/E_lizochek/clock/raw/main/src/Images/AnalogClock.png)

## Descriptions

Analog Clock implemented on React.js ([Create React App](https://github.com/facebook/create-react-app)) with using Moment.js ([Moment.js] (https://momentjs.com/)). 
Clock was made on SCSS without fancy design. 

## Project setup

In the project directory, you can run:
### npm start
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
