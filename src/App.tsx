import React from 'react';
import {Clock} from './components/Clock'
import './App.css';

function App() {
  return (
    <div className="app">
  
     <Clock title="Analog Clock"/>
    </div>
  );
}

export default App;
