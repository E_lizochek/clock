import { useEffect, useState } from 'react';
import './Clock.scss'
import moment from 'moment';

type Props = {
    title: string
}

export function Clock(props: Props): JSX.Element {

    const [second, setSecond] = useState(0);
    const [minute, setMinute] = useState(0);
    const [hour, setHour] = useState(0);

    function actualTime() {
        const currentDate = moment().toDate();

        let secondRation = currentDate.getSeconds() / 60;
        let minuteRation = (secondRation + currentDate.getMinutes()) / 60;
        let hourRation = (minuteRation + currentDate.getHours()) / 12;

        setHour(hourRation)
        setMinute(minuteRation)
        setSecond(secondRation)
    }

    useEffect(() => {
        setInterval(() => {
            actualTime()
        }, 1000)
    })

    return (
        <>
            <div className="clock">
                <div className="hour hand" style={{ transform: `rotate(${hour * 360}deg)` }} />
                <div className="minute hand" style={{ transform: `rotate(${minute * 360}deg)` }} />
                <div className="second hand" style={{ transform: `rotate(${second * 360}deg)` }} />
                <div className="numbers number1">|</div>
                <div className="numbers number2">|</div>
                <div className="numbers number3">
                    <div className='rotation3'>3</div>
                </div>
                <div className="numbers number4">|</div>
                <div className="numbers number5">|</div>
                <div className="numbers number6">
                    <div className='rotation6'>6</div>
                </div>
                <div className="numbers number7">|</div>
                <div className="numbers number8">|</div>
                <div className="numbers number9">
                    <div className='rotation9'>9</div>
                </div>
                <div className="numbers number10">|</div>
                <div className="numbers number11">|</div>
                <div className="numbers number12">12</div>
            </div>
            <div>{props.title}</div>
        </>
    )
}